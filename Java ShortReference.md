reference: 
https://www.princeton.edu/
https://www.tutorialspoint.com/
https://docs.oracle.com/

1. Editing
2. Compiling
3. Executing

# Syntax
1. Built-in data types.
    * values
    * default literals
    * typical literals
    * operation
    * operator
2. Declaration, initialization and assignment statements.
3. Operator
    * comparison operator
4. Statement
    * Flow Statement
        * Logic
        * Loop
        * Control
    * Array Statement
6. I/O
7. Functions
8. Errors Handling
9. Object Oriented Programming