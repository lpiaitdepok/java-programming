class SwapDemo1 {
	public static void main(String[] args) {
		int a = 5;
		int b = 31;
		int temp;
		System.out.println("Before swap, a = "+a+" and b = "+b);
		
		temp = a;
		a = b;
		b = temp;
		System.out.println("After swap, a = "+a+" and b = "+b);
	}
}