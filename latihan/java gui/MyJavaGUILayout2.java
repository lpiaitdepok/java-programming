import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.BorderLayout;

public class MyJavaGUILayout2 extends JFrame {
	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JButton button4;
	private JButton button5;
	
	public MyJavaGUILayout2() {
		setVisible(true);
		setSize(300,100);
		setTitle("My Java GUI");
		setLocationRelativeTo(null);
		
		button1 = new JButton("button 1");
		button2 = new JButton("button 2");
		button3 = new JButton("button 3");
		button4 = new JButton("button 4");
		button5 = new JButton("button 5");
		
		button1.setToolTipText(button1.getText());
		button2.setToolTipText(button2.getText());
		button3.setToolTipText(button3.getText());
		button4.setToolTipText(button4.getText());
		button5.setToolTipText(button5.getText());
		
		getContentPane().add(button1, BorderLayout.PAGE_START);
		getContentPane().add(button2, BorderLayout.LINE_START);
		getContentPane().add(button3, BorderLayout.CENTER);
		getContentPane().add(button4, BorderLayout.LINE_END);
		getContentPane().add(button5, BorderLayout.PAGE_END);
		pack();
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public static void main (String[] args) {
		new MyJavaGUILayout2();
	}
}