reference:
https://www.princeton.edu/
https://www.tutorialspoint.com/
www.ntu.edu.sg
https://docs.oracle.com/

# Compile-time Errors
* Syntax errors
    * ... expected
        * unclosed / unbalanced ...
        * ... is missing
    * illegal start of expression
        * extra ...
        * missing ...
    * not a statement
        * missing ...
        * ... not spelled correctly
        * ... cannot be resolved to a type
        * on token "...", invalid VariableDeclaratorId.
* Identifiers
    * cannot find symbol
    * . . . is already defined in . . .
    * . . . has private access in . . .
    * array required
* Computation
    * variable . . . might not have been initialized
    * . . . in . . . cannot be applied to . . .
    * operator . . . cannot be applied to . . . ,. . .
    * possible loss of precision
    * incompatible types
    * inconvertible types
* Return statements
    * missing return statement