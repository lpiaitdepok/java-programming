```java
Connection con = null;
Resultset rs = null;
Statement st = null;

// mysql
Class.forName("com.mysql.jdbc.Driver").newInstance();
con = DriverManager.getConnection("jdbc:mysql://HOST:3306/DATABASE","root","password");

// sqlite
Class.forName("org.sqlite.jdbc").newInstance();
con = DriverManager.getConnection("jdbc:sqlite:DATABASE.db");

// postgre
Class.forName("org.postgresql.Driver").newInstance();
con = DriverManager.getConnection("jdbc:postgresql://HOST:5432/DATABASE");

// sqlserver
Class.forName("com.microsoft.jdbc.sqlserver.SQLServerDriver").newInstance();
con = DriverManager.getConnection("jdbc:microsoft:sqlserver://HOST:1433;DatabaseName=DATABASE");

// mongodb
Class.forName("mongodb.jdbc.MongoDriver");
String URL = "jdbc:mongo://<servername>:<port>/<databaseName>";
Connection con = DriverManager.getConnection(url,"user","pass");

```