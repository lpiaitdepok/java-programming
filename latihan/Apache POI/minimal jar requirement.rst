run this code in terminal and add jar below:
--------------------------------------------

javac -cp

- poi-3.9-20121203.jar
- poi-ooxml-3.9-20121203.jar
- poi-ooxml-schemas-3.9-20121203.jar
- lib/commons-codec-1.5.jar
- lib/log4j-1.2.13.jar
- ooxml-lib/dom4j-1.6.1.jar
- ooxml-lib/stax-api-1.0.1.jar
- ooxml-lib/xmlbeans-2.3.0.jar
