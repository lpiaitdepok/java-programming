import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.GroupLayout;
import java.awt.Container;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MyJavaGUILayout12 extends JFrame {
	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JButton button4;
	private JButton button5;
	private JButton button6;
	private JButton button7;
	private JButton button8;
	private JButton button9;
	private GroupLayout group;
	private Container c;
	
	MyJavaGUILayout12() {
		
		button1 = new JButton("button 1");
		button2 = new JButton("button 2");
		button3 = new JButton("button 3");
		button4 = new JButton("button 4");
		button5 = new JButton("button 5");
		button6 = new JButton("button 6");
		button7 = new JButton("button 7");
		button8 = new JButton("button 8");
		button9 = new JButton("button 9");
		
		button1.setToolTipText(button1.getText()+getLayout().toString());
		button2.setToolTipText(button2.getText());
		button3.setToolTipText(button3.getText());
		button4.setToolTipText(button4.getText());
		button5.setToolTipText(button5.getText());
		button6.setToolTipText(button6.getPreferredSize().toString());
		button7.setToolTipText(button7.getPreferredSize().toString());
		button8.setToolTipText(button8.getPreferredSize().toString());
		button9.setToolTipText(button9.getPreferredSize().toString());
		
		c = getContentPane();
		group = new GroupLayout(c);
		c.setLayout(group);
		
		group.setHorizontalGroup(
			group.createSequentialGroup()
			.addComponent(button1)
			.addGap(10,20,100)
			.addComponent(button2)
			.addComponent(button3)
		);
		group.setVerticalGroup(
			group.createParallelGroup(GroupLayout.Alignment.BASELINE)
			.addComponent(button1)
			.addComponent(button2)
			.addComponent(button3)
		);
		
		button3.setToolTipText(c.getLayout().toString());

		setSize(400,300);
		setTitle("My Java GUI");
		setLocationRelativeTo(null);
		pack();
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
	
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}
	
	public static void main (String[] args) {
		new MyJavaGUILayout12();
	}
}