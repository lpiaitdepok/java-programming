import javax.swing.JFrame;

public class MyJavaGUI1 extends JFrame {
	
	public MyJavaGUI1() {
		setVisible(true);
		setSize(300,100);
		setTitle("My Java GUI");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public static void main (String[] args) {
		new MyJavaGUI1();
	}
}