import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MyJavaGUILayout11 extends JFrame {
	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JButton button4;
	private JButton button5;
	private JButton button6;
	private JButton button7;
	private JButton button8;
	private JButton button9;
	private GridBagLayout grid;
	private GridBagConstraints gbc;
	
	MyJavaGUILayout11() {
		
		button1 = new JButton("button 1");
		button2 = new JButton("button 2");
		button3 = new JButton("button 3");
		button4 = new JButton("button 4");
		button5 = new JButton("button 5");
		button6 = new JButton("button 6");
		button7 = new JButton("button 7");
		button8 = new JButton("button 8");
		button9 = new JButton("button 9");
		
		button1.setToolTipText(button1.getText()+getLayout().toString());
		button2.setToolTipText(button2.getText());
		button3.setToolTipText(button3.getText());
		button4.setToolTipText(button4.getText());
		button5.setToolTipText(button5.getText());
		button6.setToolTipText(button6.getPreferredSize().toString());
		button7.setToolTipText(button7.getPreferredSize().toString());
		button8.setToolTipText(button8.getPreferredSize().toString());
		button9.setToolTipText(button9.getPreferredSize().toString());
		
		grid = new GridBagLayout();
		gbc = new GridBagConstraints();
		getContentPane().setLayout(grid);
		
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.FIRST_LINE_START;
		grid.setConstraints(button1, gbc);
		getContentPane().add(button1);
		gbc.weighty = 1;
		gbc.weightx = 1;
		
		gbc.fill = GridBagConstraints.VERTICAL;
		gbc.anchor = GridBagConstraints.PAGE_START;
		gbc.gridx = 1;
		gbc.gridy = 0;
		grid.setConstraints(button2, gbc);
		getContentPane().add(button2, gbc);
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.FIRST_LINE_END;
		gbc.gridx = 2;
		gbc.gridy = 0;
		grid.setConstraints(button3, gbc);
		getContentPane().add(button3,gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.fill = GridBagConstraints.VERTICAL;
		gbc.anchor = GridBagConstraints.LINE_START;
		grid.setConstraints(button4, gbc);
		getContentPane().add(button4);
		
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.CENTER;
		grid.setConstraints(button5, gbc);
		getContentPane().add(button5);
		
		gbc.gridx = 2;
		gbc.gridy = 1;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.LINE_END;
		grid.setConstraints(button6, gbc);
		getContentPane().add(button6);
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.ipadx = 2;
		gbc.ipady = 2;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.LAST_LINE_START;
		grid.setConstraints(button7, gbc);
		getContentPane().add(button7);
		
		gbc.gridx = 1;
		gbc.gridy = 2;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.PAGE_END;
		grid.setConstraints(button8, gbc);
		getContentPane().add(button8);
		
		gbc.gridx = 2;
		gbc.gridy = 2;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.LAST_LINE_END;
		grid.setConstraints(button9, gbc);
		getContentPane().add(button9);
		
		setSize(400,300);
		setTitle("My Java GUI");
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
	}
	
	public static void main (String[] args) {
		new MyJavaGUILayout11();
	}
}