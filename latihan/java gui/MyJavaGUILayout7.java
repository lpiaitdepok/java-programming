import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import java.awt.Component;

public class MyJavaGUILayout7 extends JFrame {
	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JButton button4;
	private JButton button5;
	private JPanel panel1;
	private JPanel panel2;
	
	public MyJavaGUILayout7() {
		setVisible(true);
		setSize(150,100);
		setTitle("My Java GUI");
		setLocationRelativeTo(null);
		
		button1 = new JButton("button 1");
		button2 = new JButton("button 2");
		button3 = new JButton("button 3");
		button4 = new JButton("button 4");
		button5 = new JButton("button 5");
		
		button1.setToolTipText(button1.getText());
		button2.setToolTipText(button2.getText());
		button3.setToolTipText(button3.getText());
		button4.setToolTipText(button4.getText());
		button5.setToolTipText(button5.getText());
		button5.setToolTipText(button5.getPreferredSize().toString());
		
		button1.setAlignmentX(Component.CENTER_ALIGNMENT);
		button2.setAlignmentX(Component.CENTER_ALIGNMENT);
		button3.setAlignmentX(Component.CENTER_ALIGNMENT);
		button4.setAlignmentX(Component.CENTER_ALIGNMENT);
		button5.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		panel1 = new JPanel();
		panel1.add(button1);
		panel1.add(button2);
		panel1.add(button3);
		panel1.add(button4);
		panel1.add(button5);
		panel1.setLayout(new BoxLayout(panel1, BoxLayout.LINE_AXIS));
		panel1.setSize(80, 26);
		
		getContentPane().add(panel1);
		
		pack();
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public static void main (String[] args) {
		new MyJavaGUILayout7();
	}
}