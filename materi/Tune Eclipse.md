# Tune Eclipse Preferences
    General > Startup and Shutdown : remove all plugins activated on startup
    General > Editors > Text Editors > Spelling : Disable spell checking
    General > Validation > Suspend all
    Window > Customize Perspective > Remove stuff you don’t use or want (shortcut keys are your friends), same for Menu Visibility (how many times have you printed a source file…)
    Install/Update > Automatic Updates > Uncheck “Automatically find new updates”
    General > Appearance > Uncheck Enable Animations
    Stay with the default theme. Unfortunately, anything else makes it really laggy and slow.

    Java > Editor > Content Assist > disable Enable Auto Activation. Advanced > Remove all unwanted kinds


reference:
http://www.nicolasbize.com/
