import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException
class BufferedReaderDemo2 {
	public static void main(String[] args) {
		int a, b, c;
		try {
    		InputStreamReader r = new InputStreamReader(System.in);
    		BufferedReader br = new BufferedReader(r);
    		System.out.println("Enter 1st number: ");
    		a = Integer.parseInt(br.readLine()); /* Integer.parseInt converts string value in Integer value. */
    		System.out.println("Enter 2nd number: ");
    		b = Integer.parseInt(br.readLine());
    		c = a+b;
    		System.out.println("Addition is : " + c);
		} catch (IOException e) {
		    e.printStackTrace();
		} finally {
		    e.printStackTrace();
		}
	}
}