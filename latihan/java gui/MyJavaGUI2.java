import javax.swing.JFrame;

public class MyJavaGUI2 {
	
	public MyJavaGUI2() {
		JFrame frame = new JFrame("My Java GUI");
		
		frame.setVisible(true);
		frame.setSize(300,100);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
	}
	
	public static void main (String[] args) {
		new MyJavaGUI2();
	}
}