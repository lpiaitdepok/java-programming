import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.BorderLayout;

public class MyJavaGUILayout1 extends JFrame {
	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JButton button4;
	private JButton button5;
	
	public MyJavaGUILayout1() {
		setVisible(true);
		setSize(300,100);
		setTitle("My Java GUI");
		setLocationRelativeTo(null);
		
		button1 = new JButton("button 1");
		button2 = new JButton("button 2");
		button3 = new JButton("button 3");
		button4 = new JButton("button 4");
		button5 = new JButton("button 5");
		
		getContentPane().add(button1, BorderLayout.NORTH);
		getContentPane().add(button2, BorderLayout.WEST);
		getContentPane().add(button3, BorderLayout.CENTER);
		getContentPane().add(button4, BorderLayout.EAST);
		getContentPane().add(button5, BorderLayout.SOUTH);
		pack();
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public static void main (String[] args) {
		new MyJavaGUILayout1();
	}
}