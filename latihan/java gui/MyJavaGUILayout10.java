import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MyJavaGUILayout10 extends JFrame {
	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JButton button4;
	private JButton button5;
	private JButton button6;
	private JPanel panel1;
	private JPanel panel2;
	
	MyJavaGUILayout10() {
		
		button1 = new JButton("button 1");
		button2 = new JButton("button 2");
		button3 = new JButton("button 3");
		button4 = new JButton("button 4");
		button5 = new JButton("button 5");
		button6 = new JButton("button 6");
		
		button1.setToolTipText(button1.getText());
		button2.setToolTipText(button2.getText());
		button3.setToolTipText(button3.getText());
		button4.setToolTipText(button4.getText());
		button5.setToolTipText(button5.getText());
		button5.setToolTipText(button5.getPreferredSize().toString());
			
		getContentPane().setLayout(new GridLayout(2, 3));
		
		getContentPane().add(button1);
		getContentPane().add(button2);
		getContentPane().add(button3);
		getContentPane().add(button4);
		getContentPane().add(button5);
		getContentPane().add(button6);

		setSize(150,100);
		setTitle("My Java GUI");
		setLocationRelativeTo(null);
		pack();
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
	}
	
	public static void main (String[] args) {
		MyJavaGUILayout10 mj = new MyJavaGUILayout10();
	}
}