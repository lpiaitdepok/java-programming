rem NPP_SAVE
cd $(CURRENT_DIRECTORY)
javac $(FILE_NAME)
IF $(EXITCODE) != 0 GOTO EXITSCRIPT
NPP_SETFOCUS Con
java -cp . $(NAME_PART)
:EXITSCRIPT

rem compile and run java using class (example: using sqlite jdbc)
rem NPP_SAVE
cd $(CURRENT_DIRECTORY)
javac $(FILE_NAME)
IF $(EXITCODE) != 0 GOTO EXITSCRIPT
java -cp .;"./sqlite-jdbc-3.23.1.jar" $(NAME_PART)
rem . dot is current directory
rem when using linux change ; semicolon to : colon
:EXITSCRIPT