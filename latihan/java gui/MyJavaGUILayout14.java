import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import java.awt.FlowLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MyJavaGUILayout14 extends JFrame {
	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JButton button4;
	private JButton button5;
	private Container c;
	private JScrollPane scroll;
	private JPanel panel;
	
	MyJavaGUILayout14() {
		
		button1 = new JButton("button 1");
		button2 = new JButton("button 2");
		button3 = new JButton("button 3");
		button4 = new JButton("button 4");
		button5 = new JButton("button 5");
		
		// button1.setSize(new Dimension(400,200));
		button1.setPreferredSize(new Dimension(70,80));
		
		button1.setToolTipText(button1.getText()+getLayout().toString());
		button2.setToolTipText(button2.getText());
		button3.setToolTipText(button3.getText());
		button4.setToolTipText(button4.getText());
		button5.setToolTipText(button5.getText());
		
		c = getContentPane();
		
		panel = new JPanel();
		panel.setLayout(new FlowLayout());
		
		panel.add(button1);
		panel.add(button2);
		panel.add(button3);
		panel.add(button4);
		panel.add(button5);
		
		button3.setToolTipText(button3.getToolTipText()+"\r\n\r\n"+c.getLayout());
		
		scroll = new JScrollPane(panel, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		c.add(scroll);
		
		setTitle("My Java GUI");
		pack();
		setSize(400,300);
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
	}
	
	public static void main (String[] args) {
		new MyJavaGUILayout14();
	}
}