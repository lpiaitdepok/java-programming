import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.SpringLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MyJavaGUILayout13 extends JFrame {
	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JButton button4;
	private JButton button5;
	private JButton button6;
	private JButton button7;
	private JButton button8;
	private JButton button9;
	private SpringLayout spring;
	private Container c;
	
	MyJavaGUILayout13() {
		
		button1 = new JButton("button 1");
		button2 = new JButton("button 2");
		button3 = new JButton("button 3");
		button4 = new JButton("button 4");
		button5 = new JButton("button 5");
		button6 = new JButton("button 6");
		button7 = new JButton("button 7");
		button8 = new JButton("button 8");
		button9 = new JButton("button 9");
		
		// button1.setSize(new Dimension(400,200));
		button1.setPreferredSize(new Dimension(70,80));
		
		button1.setToolTipText(button1.getText()+getLayout().toString());
		button2.setToolTipText(button2.getText());
		button3.setToolTipText(button3.getText());
		button4.setToolTipText(button4.getText());
		button5.setToolTipText(button5.getText());
		button6.setToolTipText(button6.getPreferredSize().toString());
		button7.setToolTipText(button7.getPreferredSize().toString());
		button8.setToolTipText(button8.getPreferredSize().toString());
		button9.setToolTipText(button9.getPreferredSize().toString());
		
		c = getContentPane();
		spring = new SpringLayout();
		c.setLayout(spring);
		
		c.add(button1);
		c.add(button2);
		c.add(button3);
		c.add(button4);
		c.add(button5);
		c.add(button6);
		
		spring.putConstraint(SpringLayout.WEST, button1, 5, SpringLayout.NORTH, c);
		spring.putConstraint(SpringLayout.WEST, button2, 80, SpringLayout.WEST, c);
		spring.putConstraint(SpringLayout.EAST, button3, 5, SpringLayout.EAST, c);
		spring.putConstraint(SpringLayout.NORTH, button4, 5, SpringLayout.HORIZONTAL_CENTER, c);
		spring.putConstraint(SpringLayout.WEST, button5, 5, SpringLayout.HORIZONTAL_CENTER, c);
		spring.putConstraint(SpringLayout.NORTH, button6, 5, SpringLayout.VERTICAL_CENTER, c);
		
		button3.setToolTipText(button3.getToolTipText()+"\r\n\r\n"+c.getLayout());
		
		setTitle("My Java GUI");
		pack();
		setSize(400,300);
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
	}
	
	public static void main (String[] args) {
		new MyJavaGUILayout13();
	}
}