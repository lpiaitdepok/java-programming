import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.Container;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MyJavaGUILayout9 extends JFrame implements ActionListener{
	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JButton button4;
	private JButton button5;
	private Container c;
	private CardLayout card;
	
	MyJavaGUILayout9() {
		
		button1 = new JButton("button 1");
		button2 = new JButton("button 2");
		button3 = new JButton("button 3");
		button4 = new JButton("button 4");
		button5 = new JButton("button 5");
		
		button1.addActionListener(this);
		button2.addActionListener(this);
		button3.addActionListener(this);
		button4.addActionListener(this);
		button5.addActionListener(this);
		
		button1.setToolTipText(button1.getText());
		button2.setToolTipText(button2.getText());
		button3.setToolTipText(button3.getText());
		button4.setToolTipText(button4.getText());
		button5.setToolTipText(button5.getText());
		button5.setToolTipText(button5.getPreferredSize().toString());
			
		c = getContentPane();
		card = new CardLayout(40, 30);
		
		c.setLayout(card);
		c.add("a", button1);
		c.add("b", button2);
		c.add("c", button3);
		c.add("d", button4);
		c.add("e", button5);

		setSize(150,100);
		setTitle("My Java GUI");
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public void actionPerformed(ActionEvent e) {
		card.next(c);
	}
	
	public static void main (String[] args) {
		MyJavaGUILayout9 mj = new MyJavaGUILayout9();
	}
}