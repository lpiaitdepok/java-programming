import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.ScrollPaneConstants;
import java.awt.FlowLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MyJavaGUILayout15 extends JFrame {
	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JButton button4;
	private JButton button5;
	private Container c;
	
	MyJavaGUILayout15() {
		
		button1 = new JButton("button 1");
		button2 = new JButton("button 2");
		button3 = new JButton("button 3");
		button4 = new JButton("button 4");
		button5 = new JButton("button 5");
				
		button1.setToolTipText(button1.getText()+getLayout().toString());
		button2.setToolTipText(button2.getText());
		button3.setToolTipText(button3.getText());
		button4.setToolTipText(button4.getText());
		button5.setToolTipText(button5.getText());
		
		c = getContentPane();
		
		button3.setToolTipText(button3.getToolTipText()+"\r\n\r\n"+c.getLayout());
		
		c.setLayout(null);
		c.add(button1);
		c.add(button2);
		c.add(button3);
		c.add(button4);
		c.add(button5);
		
		Insets insets = c.getInsets();
		Dimension size = button1.getPreferredSize();
		button1.setBounds(25 + insets.left, 5 + insets.top, size.width, size.height);
		size = button2.getPreferredSize();
		button2.setBounds(55 + insets.left, 40 + insets.top, size.width, size.height);
		size = button3.getPreferredSize();
		button3.setBounds(150 + insets.left, 15 + insets.top, size.width+50, size.height+20);
		
		setTitle("My Java GUI");
		pack();
		insets = getInsets();
		setSize(300 + insets.left + insets.right, 125 + insets.top + insets.bottom);
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
	}
	
	public static void main (String[] args) {
		new MyJavaGUILayout15();
	}
}